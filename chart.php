<html><head>
<title>Google visualisation demo: chart query controls</title>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

  // Load the Visualization API and the controls package.
  // Packages for all the other charts you need will be loaded
  // automatically by the system.
  google.load('visualization', '1.1', {'packages':['controls','linechart']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.setOnLoadCallback(initialize);

function initialize() {
  // Replace the data source URL on next line with your data source URL.
  var query = new google.visualization.Query('http://spreadsheets.google.com/tq?tq=select%20A%2CE%20where%20E%20%3C%20200&key=0AmbQbL4Lrd61dG1pZ0RITHRWSGVCLXJCOGl2OG5tMlE&gid=4');
  
  
  // Send the query with a callback function.
  query.send();
}


</script>

</head>
<body>

<!--Div that will hold the dashboard-->
    <div id="dashboard_div">
      <!--Divs that will hold each control and chart-->
      <div id="filter_div"></div>
      <div id="chart_div"></div>
    </div>
    
<body>
</html>