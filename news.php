<?php include('includes/header.php'); ?>
	
	<header>
		<div class="logo" style="background:url('webimages/localshares-logo-reverse.png');"></div>
		
		<h1>News</h1>
	</header>
		
	<?php include('inc_nav.php'); ?>
	
	<div class="group"></div>
	
	<div class="content subPage">
		<div class="text">
			<h2>In The News</h2>
			
			<div class="newsList">
				<div class="newsItem">
					<a href="#" class="title">Press Release: Localshares Expands With New Leadership, Divisions <span class="date">2/02/16</span></a>
					<p class="source">Press Release</p>
					<a href="#" class="readMore">Read More > </a>
				</div>
				
				<div class="newsItem">
				<a href="#" class="title">11 Metro Nashville 8Th Graders Complete Competitive Gateway To Business Fellowship; Discover Paths To Career <span class="date">5/28/15</span></a>
					<p class="source">Advisor.ca</p>
					<span class="desc">
						<p>Please <a href="#">click here</a> to view the fund performance, and holdings.
							<br />Please <a href="#">click here</a> to view current fund's prospectus.
							<br />Past performance is no guarantee of future returns.
							<br />Holdings are subject to change.</p>
					</span>
					<a href="#" class="readMore">Read More > </a>
				</div>
				
			</div><!--end newsList-->
		</div><!--end text-->
		
		<div class="sidebar">
						
			<a href="#" class="textLink">Archives ></a>
			
			<?php include('inc_mission.php'); ?>
			
			
		</div><!--end sidebar-->	
	</div><!--end content-->

<?php include('includes/footer.php'); ?>