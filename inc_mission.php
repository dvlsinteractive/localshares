<div class="mission">
	<h2>Mission</h2>
	<p>To optimize investment in local economies while aligning business, community and government to elevate the whole.</p>
	<span class="leaf" style="background:url('webimages/leaf-icon.png');"></span>
</div>