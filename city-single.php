<?php include('includes/header.php'); ?>
	
	<header>
		<div class="logo" style="background:url('webimages/localshares-logo-reverse.png');"></div>
	</header>
		
	<?php include('inc_nav.php'); ?>
	
	<div class="group"></div>
	
	<div class="content subPage">
		<div class="sidebar">			
			<img src="webimages/localshares_cities-los-angeles.png" />
			
			<h2 class="company">Sample Portfolio Companies</h2>
			
			<div class="companies">
				<span class="header">
					<span class="left">Symbol</span>
					<span class="right">Name</span>
					<div class="group"></div>
				</span>
				
				<span class="lineVert"></span>
				
				<span class="row group">
					<span class="left">ABC</span>
					
					<span class="right">ABC Company</span>
				</span>
				
				<span class="row group">
					<span class="left">ABC</span>
					
					<span class="right">ABC Company</span>
				</span>
				
				<span class="row group">
					<span class="left">ABC</span>
					
					<span class="right">ABC Company</span>
				</span>
			</div><!--end companies-->
			
			<div class="news">
				<h2 class="company">News</h2>
				<span class="news-content">
				<p>Pittsburgh finished tops among 25 key U.S. cities between the close of Dec. 30, 2015 and March 31, 2016, according to new stock-market indices that measure stock-price changes of companies headquartered in those cities. The town perhaps better known for its famed NFL football team saw its index rose 25.7% during the period, measured by investment manager LocalShares, topping the 0.2% decline by the Standard & Poor's 500.</p>
				</span>
			</div>
		</div><!--end sidebar-->
		
		<div class="text">
			<div class="chart-line">
				<h2><span>City</span> Sample Portfolio vs...</h2> 
				<iframe width="600" height="371" seamless frameborder="0" scrolling="yes" src="https://docs.google.com/spreadsheets/d/1Q90AzfdcgGZi0BmFEWiYZkoRVdRUOg29-YHfxfvb5hs/pubchart?oid=1993097261&amp;format=interactive"></iframe>
			</div>
			
			<div class="chart-donut">
				<h2>Index/Industry Breakdown</h2>
				<iframe width="696" height="430" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1B14ehY75J2p0UYQo-_eHr09f01J4M9_IQ_8eSkmXX1o/pubchart?oid=1178958746&amp;format=interactive"></iframe>
			</div>
									
		</div><!--end text-->
		
		
	</div><!--end content-->

<?php include('includes/footer.php'); ?>