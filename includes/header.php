<?php
	// $cookie_name = "home";
	// $cookie_value = "visited";
	// setcookie($cookie_name, $cookie_value, time() + (86400 * 7), "/"); // 86400 = 1 day
	
	// unset cookie
	// setcookie('home', null, -1, '/');
	
	// if(!isset($_COOKIE['home'])) {
		
	$banner = true;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="title" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<title>LocalShares</title>

	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon" href="apple-touch-icon-114x114.png">

	<link rel='stylesheet' href='css/layout.css' />
	

<!-- remove before production -->
<script src="http://localhost:35729/livereload.js?snipver=1"></script>

<!-- gulp/sass error styles -->
<style>
	body:before {
		background:#c0392b;
		display: block;
		color:#fff;
		padding:2%;
	}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mmenu.all.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('nav#menu').mmenu();
	});
</script>


<link href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
<link href="ticker/css/jquery.stockticker.css" rel="stylesheet" type="text/css" />
<script src="ticker/js/jquery.stockticker.js" type="text/javascript" ></script>

</head>

<body id="">
	

	<nav id="menu" class="pageLoad">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="contact.php">Contact</a></li>
			<li><a href="localshares-cities.php"><em>LocalShares</em> Cities</a>
				<ul class="sub">
					<li><a href="austin.php">Austin</a></li>
					<li><a href="#_">Dallas</a></li>
					<li><a href="#_">Houston</a></li>
					<li><a href="#_">Atlanta</a></li>
					<li><a href="#_">Tampa</a></li>
				</ul>
			</li>
		</ul>
	</nav>
	
	
	<?php if($banner == true) { ?>
	<div class="homeBanner">
		<span class="center">
		<p>Information regarding the Nashville Area ETF previously found on this URL can now be found at <a href="http://www.nashvilleareaetf.com" rel="external">www.Nashvilleareaetf.com</a> or by clicking below on “city-specific ETFS.”</p>
<!--
		<a href="javascript:void(0);" class="close">
			<img src="webimages/close.svg" />
		</a>
-->
		</span>
	</div>
	<?php } ?>
	
	
<div class="wrapper">
