<?php 
	$carousel = false;	
?>

<?php include('includes/header.php'); ?>
	
	<div class="carousel">
		<?php if($carousel == false) { ?>
		<a href="#" class="noSlide">
			<img src="webimages/slide-localshares_cities.jpg" />
		</a>
		<?php } else { ?>
		
		<div class="slides">
			<span>
				<a href="http://www.google.com">
				<img src="webimages/slide-localshares_cities.jpg" />
				</a>
			</span>
			
			<span>
				<img src="webimages/slide-nashville_eft.jpg" />
			</span>
			
			<span>
				<img src="webimages/slide-new_leadership.jpg" />
			</span>
		</div><!--end slides-->
		
		<div class="sliderNav">
			<div class="pagination"></div>
			
			<div class="navigation">
				<a href="javascript:void(0);" class="prev" style="background:url('webimages/arrow-left.svg');">previous slide</a>
				
				<a href="javascript:void(0);" class="next" style="background:url('webimages/arrow-right.svg');">next slide</a>
			</div>
		</div><!--end navigation-->
		
		<?php } ?>
	</div><!--end carousel-->
	
	<?php include('inc_nav.php'); ?>
	
	<div class="group"></div>
	
	<div class="content">
		<div class="sidebar">
			<div class="logo">
				<img src="webimages/localshares-logo.png" />
			</div>
			
			<?php include('inc_mission.php'); ?>
			
			<?php include('inc_eftlink.php'); ?>
		</div>
		
		<div class="featuredText">
			<h2>Investment Division <em>Objectives</em></h2>
			
			<p><img src="webimages/chart-icon.jpg"  /> LocalShares illuminates the economic vitality of key American cities based on the performance of their leading publicly-traded, headquartered companies. Our indices concentrate on companies with strong performance and excellent liquidity, located in dynamic and economically diverse metropolitan statistical areas. LocalShares emphasizes city economies that are pro-business and add competitive value to the companies that choose to locate there.</p>
		</div>
		
		<div class="featuredText">
			<h2>Knowledge Division <em>Objectives</em></h2>
			
			<p><img src="webimages/library-icon.jpg" /> LocalShares elevates the profile and supports the economic prosperity of pro-business American cities. We accomplish this by offering education and information, including evolving market data, to drive financial and non-financial returns on investments in local economies.  LocalShares showcases a spectrum of opportunities for local investment through corporate social responsibility initiatives; conferences; and other events.</p>
		</div>
		
		
	</div><!--end content-->

<?php include('includes/footer.php'); ?>