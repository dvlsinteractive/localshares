<?php include('includes/header.php'); ?>
	
	<header>
		<div class="logo" style="background:url('webimages/localshares-logo-reverse.png');"></div>
		
		<h1 class="lower">LocalShares Cities</h1>
	</header>
		
	<?php include('inc_nav.php'); ?>
	
	<div class="group"></div>
	
	<div class="content subPage">
		<div class="sidebar">			
			<div class="grayBox">
				<h3>Our Cities</h3>
				<h4>Index Inclusion Criteria</h4>
				<p>U.S. companies selected for inclusion in a LocalShares Area Index must:</p>
				<ul>
					<li>be headquartered in the specific MSA</li>
					<li>have $100 million or more in market capitalization</li>
					<li>50,000 average trading volume over the previous 90 days</li>
				</ul>
			</div><!--end textBox-->
			
			<div class="mapLink">
				<a href="webimages/map-lg.jpg" class="popup">
					<img src="webimages/map.png" />
				</a>
				<p>click interactive map to enlarge</p>
			</div>
			
			<?php include('inc_eftlink.php'); ?>
		</div><!--end sidebar-->
		
		<div class="text">
			<img src="webimages/cities-chart.jpg" />
			<br />
			
			<div class="cityLinks">
				<a href="#">
					<img src="webimages/localshares_cities-los-angeles.png" />
				</a>
				
				<a href="#">
					<img src="webimages/localshares_cities-los-angeles.png" />
				</a>
				
				<a href="#">
					<img src="webimages/localshares_cities-los-angeles.png" />
				</a>
				
				<a href="#">
					<img src="webimages/localshares_cities-los-angeles.png" />
				</a>
				
				<a href="#">
					<img src="webimages/localshares_cities-los-angeles.png" />
				</a>
				
				<a href="#">
					<img src="webimages/localshares_cities-los-angeles.png" />
				</a>
				
			</div>
						
		</div><!--end text-->
		
		<div class="text full">
			<iframe width="1069.659574468085" height="661.5" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1Q90AzfdcgGZi0BmFEWiYZkoRVdRUOg29-YHfxfvb5hs/pubchart?oid=1376064663&amp;format=interactive"></iframe>
		</div>
		
	</div><!--end content-->

<?php include('includes/footer.php'); ?>