<a href="#modal" class="popup-modal etfLink">
	<img src="webimages/nashvilleetf.png" />
	Click here to learn more about city-specific ETFs
</a>


<div id="modal" class="mfp-hide">
	<p>By clicking OK you will be leaving <a href="http://www.localsharesllc.com">LocalSharesLLC.com</a> and acknowledge doing so.</p>
	
	<span class="buttons">
	<a href="http://www.localshares.com/nashvilleetf.aspx">OK</a>
	<a href="#_" onclick="magnificPopup.close();">Cancel</a>
	</span>
</div>