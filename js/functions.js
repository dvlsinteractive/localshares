$(document).ready(function() {
	// homepage carousel
	var owl = $('.carousel > .slides');
	
	owl.owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots: true,
    dotsContainer: '.sliderNav .pagination',
    smartSpeed: 400,
    responsive:{
        0:{
            items:1
        },
        1:{
            items:1
        }
    }
	});
	
		
	// slider nav links
	$('.sliderNav a.prev').click(function() {
		owl.trigger('prev.owl.carousel');
	});
	
	$('.sliderNav a.next').click(function() {
		owl.trigger('next.owl.carousel');
	});
	
	
	// lightbox
	$('.popup').magnificPopup({
		disableOn: 800,
		mainClass: 'mfp-fade',
		type: 'image',
		fixedContentPos: false,
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		gallery: {
      enabled:true
    }
	});	
  
  $('.popup-youtube').magnificPopup({
		disableOn: 800,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});	
	
	$('.popup.gallery').magnificPopup({
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    }
  });
  
  
  $('.popup-modal').magnificPopup({
		mainClass: 'mfp-fade',
		type: 'inline',
		fixedContentPos: false,
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile'
	});
	
	
	// open links in new window
	$('a[rel="external"]').click(function(){
	    window.open(this.href);
	    return false;
	});
	
	
	
	// homepage banner	
	$('.homeBanner a.close').click(function(){
		$('.homeBanner').addClass('offScreen');
	});

	
	
});


$( window ).load(function() {
  $('nav#menu').removeClass('pageLoad');
});