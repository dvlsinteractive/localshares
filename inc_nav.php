<nav class="main">
	<a href="#menu" class="mobileNavLink">
		<img src="webimages/menu.svg" />
		<span class="linkText">Menu</span>
	</a>
	
	<ul>
		<li><a href="index.php">Home</a></li>
		<li><a href="about.php">About</a></li>
		<li><a href="news.php">News</a></li>
		<li><a href="contact.php">Contact</a></li>
		<li><a href="localshares-cities.php"><em>LocalShares</em> Cities</a>
			<!--ul class="sub">
				<li><a href="austin.php">Austin</a></li>
				<li><a href="#_">Dallas</a></li>
				<li><a href="#_">Houston</a></li>
				<li><a href="#_">Atlanta</a></li>
				<li><a href="#_">Tampa</a></li>
			</ul-->
		</li>
	</ul>
</nav>