// load Gulp dependencies
var gulp = require('gulp'); 
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var cmq = require('gulp-combine-media-queries');
var uglify = require('gulp-uglify');
var cssbeautify = require('gulp-cssbeautify');
var livereload = require('gulp-livereload');


function onError(err) {
    console.log(err);
}


// watch SCSS
gulp.task('styles', function () {

  // compile scss
	return sass('sass/main.scss')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    
    // automatically add needed browser prefixing
    .pipe(autoprefixer({
        browsers: ['> 5%', 'IE 9','last 2 versions'],
        cascade: false,
        remove: true
    }))
    
    // combine media queries/move to end of document
    .pipe(cmq({
      log: true
    }))
    
     // minify/compress css
    .pipe(minifyCss({
	    processImport: false,
	    debug: true,
	    keepBreaks: true
	  }))
    
    
	  // uncomment below to parse uncompressed, editable css file
		.pipe(cssbeautify({
	    indent: '  ',
	    openbrace: 'end-of-line',
	    autosemicolon: true
	  }))
	  
	  // rename in folder
		.pipe(rename('layout.css'))			
    .pipe(gulp.dest('css'))
    
    // re-inject styles into page
    .pipe(livereload());
});




// watch javascript
gulp.task('scripts', function() {
    return gulp.src('js/functions.js')
    	.pipe(uglify())
    	.pipe(rename('functions.min.js'))			
			.pipe(gulp.dest('js'))
      .pipe(livereload());
});




// watch files for changes
gulp.task('watch', function() {	
	livereload.listen();

  //watch and reload PHP and html
  gulp.watch('**/*.php').on('change', function(file) {
  	livereload.changed(file.path); 
  });
  
  gulp.watch('**/*.html').on('change', function(file) {
  	livereload.changed(file.path);
 	});

  gulp.watch('js/*.js', ['scripts']);
  gulp.watch('sass/*.scss', ['styles']);
});



// Ready? Set... Go!
gulp.task('default', ['styles', 'scripts', 'watch']);