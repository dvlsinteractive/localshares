<?php include('includes/header.php'); ?>
	
	<header>
		<div class="logo" style="background:url('webimages/localshares-logo-reverse.png');"></div>
		
		<h1>About</h1>
	</header>
		
	<?php include('inc_nav.php'); ?>
	
	<div class="group"></div>
	
	<div class="content subPage">
		<div class="text">
			<h2>History</h2>
			<p>Founded in 2010 by Elizabeth S. Courtney, William Decker and Michael Shmerling, LocalShares provides investment in and knowledge about local economies - such as cities -  by providing investment services, information and products.</p>
 
			<p>According to a study reported in the <em>Journal of Finance</em> - the average American investor holds 30% of his/her portfolio in companies located within 250 miles of his/her home.  There is a strong, proven preference for local investing and a comparable interest in knowledge about locally-headquartered companies. Yet few if any companies provide direct access to these important economies via knowledge or investment vehicles.</p>
 
			<p>Our leadership has spent 10 years researching and developing the platform for this new frontier.</p>
			
			<div class="management">
				<h2>Management</h2>
				
				<div class="bio">
					<img src="webimages/margaret-dolan.png" />
					<h3>Margaret O. Dolan</h3>
					<h4>President, Knowledge Division</h4>
					<a href="#_" class="email">Email</a>
					<p>Margaret O. Dolan, president of the Knowledge Division, is a prominent Nashville business and community leader and formerly President and CEO of the Saint Thomas Health Foundations. Dolan, who also formerly served as the vice president of community relations for Ingram Industries Inc., brings a wealth of experience building and working in partnerships with organizations to capitalize on the interdependence of business and community improvement strategy. Dolan currently serves on the boards of Fifth Third Bank Tennessee, the Nashville Area Chamber of Commerce, the Nashville Business Coalition, the Tennessee Business Roundtable, and is a previous chair and current director for both the Nashville Public Education Foundation and the United Way of Metropolitan Nashville. Dolan, a Certified Public Accountant, is a graduate of the University of Tennessee and received her Master of Business Administration from Vanderbilt's Owen Graduate School of Management, where she received the Dean Martin S. Geisel Leadership Award. Dolan was recognized with the Nelson C. Andrews Distinguished Service Award and in 2015, she was inducted into the prestigious Academy for Women of Achievement by the YWCA of Middle Tennessee.</p>
				</div>
			</div>
		</div><!--end text-->
		
		<div class="sidebar">
			
			<?php include('inc_mission.php'); ?>
			
			<div class="textBox">
				<p><strong>Board of Directors</strong></p>
				<ul>
					<li>Michael D. Shmerling, Chairman</li>
					<li>Elizabeth Seigenthaler Courtney</li>
					<li>William S. Decker</li>
					<li>Jim Phillips</li>
					<li>Cam Newton</li>
				</ul>
			</div><!--end textBox-->
			
		</div><!--end sidebar-->	
	</div><!--end content-->

<?php include('includes/footer.php'); ?>